package com.kshrd.mornsreynich_hw_001;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ShowActivity extends AppCompatActivity {
        ImageView imgView;
        Bundle bundle;
        TextView text;
        EditText editText;
@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        imgView=findViewById(R.id.imgView);
        text=findViewById(R.id.text);

        bundle=getIntent().getExtras();

        if(bundle!=null){
        String name=bundle.getString("name");
        setUp(name);
        }
        }

private void setUp(String name) {
        if(name.equals("imgFirst")){
        imgView.setImageResource(R.drawable.first);
        text.setText("Android is a mobile operating system based on\n" +
        "        a modified version of the Linux kernel and other open source software,\n" );
        }
        else {
        imgView.setImageResource(R.drawable.second);
        text.setText("he cat is a beautiful animal with an independent nature, fluffy and soft\n");
        }
        }

        Intent intent=new Intent();
public void btnSend(View view) {
        editText=findViewById(R.id.editText);
        Toast.makeText(ShowActivity.this, editText.getText(), Toast.LENGTH_SHORT).show();

        String stringToPassBack = editText.getText().toString();
        intent.putExtra("keyName", stringToPassBack);
        setResult(RESULT_OK, intent);
        finish();

        }
        }