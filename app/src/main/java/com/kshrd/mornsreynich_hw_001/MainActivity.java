package com.kshrd.mornsreynich_hw_001;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imgFirst,imgSecond;
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgFirst=findViewById(R.id.imgFirst);
        imgSecond=findViewById(R.id.imgSecond);
        text=findViewById(R.id.text);

        imgFirst.setOnClickListener(this);
        imgSecond.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent=new Intent(MainActivity.this,ShowActivity.class);
        switch (v.getId()){
            case R.id.imgFirst:
                intent.putExtra("name","imgFirst");
                startActivity(intent);
                break;
            case R.id.imgSecond:
                intent.putExtra("name","imgSecond");
                startActivity(intent);
                break;
        }

    }
}